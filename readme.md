> Debugging functions for Joomla developers as System Plugin.

> Debug output is only visible for configured IPs (usefull on live sites).

> examples:

- domix($obj); (print)
- domixM($obj) (mail to configured mail)
- domixD($obj); (print var_dump)
- domixDB(); (database debugging, output sql query for copy/paste and errors)
- domixCT(); (function call trace)